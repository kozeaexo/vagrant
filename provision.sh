#!/bin/bash

# Display a welcoming message
echo ""
echo "🌟 Welcome to the Vagrant Configuration Script 🌟"
echo "________________________________________________"
echo ""

# Copy the private SSH key to Vagrant's user directory
echo "🔑 Adding Private SSH Key to Vagrant"
cp /home/vagrant/ssh/id_rsa /home/vagrant/.ssh/
echo "________________________________________________"
echo ""

# Add GitHub to known_hosts
echo "🔍 Adding GitHub to known_hosts"
ssh-keyscan -H github.com >> /home/vagrant/.ssh/known_hosts
echo "________________________________________________"
echo ""

# Test SSH connection to GitHub
echo "🔗 Testing SSH connection to GitHub"
ssh -T git@github.com  || true
echo "________________________________________________"
echo ""

# Installing gitlab-ci-local
echo "🛠️ Installing gitlab-ci-local"
sudo wget -O /etc/apt/sources.list.d/gitlab-ci-local.sources https://gitlab-ci-local-ppa.firecow.dk/gitlab-ci-local.sources
sudo apt-get update
sudo apt-get install -y gitlab-ci-local
echo "________________________________________________"
echo ""

# Testing gitlab-ci-local version
echo "📋 Testing gitlab-ci-local version"
gitlab-ci-local --version
echo "________________________________________________"
echo ""

# Change Bash prompt color
echo "🌈 Changing terminal prompt color"
echo 'PS1="\[\e[32m\]┌──(\[\e[94;1m\]\u\[\e[94m\]@\[\e[94m\]\h\[\e[0;32m\])-[\[\e[38;5;46;1m\]\w\[\e[0;32m\]] [\[\e[32m\]$?\[\e[32m\]]\n\[\e[32m\]╰─\[\e[94;1m\]\$\[\e[0m\] "' >> /home/vagrant/.bashrc
source /home/vagrant/.bashrc
echo "________________________________________________"
echo ""

# Display a success message
echo "🎉 Configuration completed successfully! Enjoy your Vagrant environment!"
echo ""
