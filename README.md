
# Vagrant Project

This project utilizes Vagrant to manage virtual machine environments for development purposes.

## Description

The purpose of this project is to streamline the setup and configuration of virtual machines using Vagrant. It provides a simple Makefile and shell scripts to automate common tasks such as starting, provisioning, and rebooting the VM.

Additionally, it installs GitLab CI Runner locally for continuous integration tasks.

## Prerequisites

- **VirtualBox** : Version 7.0.18
- **Vagrant** : Version 2.4.1

## Usage

### Installation

To start the VM and provision it with necessary configurations:

```bash
make install
```

### Serve

To start the VM without provisioning:

```bash
make serve
```

### Reboot

To destroy the current VM, start a new one, provision it, and SSH into it:

```bash
make reboot
```

## Directory Structure

- **Vagrantfile**: Configuration file for defining the virtual machine environment.
- **provision.sh**: Dontaining shell scripts used for provisioning and other tasks.
- **Makefile**: Makefile with targets for common operations.
