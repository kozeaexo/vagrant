VAGRANT_UP = vagrant up
VAGRANT_SSH = vagrant ssh
VAGRANT_DESTROY = vagrant destroy -f

install:
	@echo "🚀 Starting VM and provisioning..."
	$(VAGRANT_UP) --provision && $(VAGRANT_SSH)

serve:
	@echo "🔧 Starting VM..."
	$(VAGRANT_UP) && $(VAGRANT_SSH)

reboot:
	@echo "🔄 Rebooting VM..."
	$(VAGRANT_DESTROY) && $(VAGRANT_UP) --provision && $(VAGRANT_SSH)
